package gusl.rule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BiRequest {
    private String kpi;
    // params
    private FromToMappings fieldMappings;
    private String responseEvent;

    @Override
    public String toString() {
        return "BiRequest " + kpi;
    }

}
