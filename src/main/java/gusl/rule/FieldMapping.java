package gusl.rule;

import gusl.rule.conditions.FieldMappingType;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@EqualsAndHashCode
public class FieldMapping {
    private String field;
    private FieldMappingType type;
    private String uiName;
}
