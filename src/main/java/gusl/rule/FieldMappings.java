package gusl.rule;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class FieldMappings {

    @Singular
    private List<FieldMapping> mappings;

    public void addMapping(FieldMapping mapping) {
        if (mappings == null) {
            mappings = new ArrayList<>(5);
        }
        mappings.add(mapping);
    }
}
