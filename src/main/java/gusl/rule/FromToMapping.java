package gusl.rule;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class FromToMapping {
    private FieldMapping from;
    private FieldMapping to;
}
