package gusl.rule;

import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class FromToMappings {

    @Singular
    private List<FromToMapping> mappings;
}
