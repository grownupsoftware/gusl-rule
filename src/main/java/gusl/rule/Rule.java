package gusl.rule;

public interface Rule {
    String getId();

    String getName();

}
