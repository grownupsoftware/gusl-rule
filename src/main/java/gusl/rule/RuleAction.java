package gusl.rule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

import static java.util.Objects.isNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RuleAction {

    private String id;

    @NotBlank
    private RuleActionType type;

    // one of:
    private SendEvent sendEvent;
    private UpdateAction updateAction;
    private BiRequest biRequest;

    @Override
    public String toString() {
        return "Action: "
                + (type == RuleActionType.NO_OP ? "NO_OP" : "")
                + (isNull(sendEvent) ? "" : sendEvent.toString())
                + (isNull(updateAction) ? "" : updateAction.toString())
                + (isNull(biRequest) ? "" : biRequest.toString());
    }
}
