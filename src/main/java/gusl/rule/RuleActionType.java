package gusl.rule;

public enum RuleActionType {
    SEND_EVENT,
    UPDATE_VALUE,
    BI_REQUEST,
    NO_OP
}
