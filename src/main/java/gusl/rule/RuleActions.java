package gusl.rule;

import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class RuleActions {

    @Singular
    private List<RuleAction> actions;
}
