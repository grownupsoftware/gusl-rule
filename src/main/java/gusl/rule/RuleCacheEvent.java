package gusl.rule;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class RuleCacheEvent extends AbstractCacheActionEvent<RuleDO> {

    public RuleCacheEvent() {
    }

    public RuleCacheEvent(CacheAction action, RuleDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
    
