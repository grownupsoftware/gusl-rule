package gusl.rule;

import gusl.rule.conditions.Condition;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class RuleConditions {

    @Singular
    private List<Condition<?>> conditions;

    private RuleConditions or;
}
