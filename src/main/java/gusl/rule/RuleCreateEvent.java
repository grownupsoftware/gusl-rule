package gusl.rule;

import gusl.model.Identifiable;
import lombok.*;

@Getter
@Setter
@Builder(toBuilder = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RuleCreateEvent implements Identifiable<String> {
    private String id;
    private RuleDO rule;
}
