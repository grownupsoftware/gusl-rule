package gusl.rule;

import gusl.core.annotations.DocField;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode
public class RuleDO {

    @DocField(description = "Database version - used by Evolutions migrate tool")
    public static final int VERSION = 1;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    @DocField(description = "Unique id")
    private String id;

    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    @DocField(description = "Date created")
    private ZonedDateTime dateCreated;

    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    @DocField(description = "Date last modified")
    private ZonedDateTime dateUpdated;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Name / Description of rule")
    private String name;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    @DocField(description = "Condition")
    private RuleConditions whenConditions;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    @DocField(description = "Then actions")
    private RuleActions thenActions;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Status of the rule")
    private RuleStatus status;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Rule category")
    private String category;

    // Could be a list...
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String trigger;

}
