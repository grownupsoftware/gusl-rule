package gusl.rule;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

@Value
@Builder
@EqualsAndHashCode
public class RuleLookupResponse {

    List<RuleData> rules;

    static RuleLookupResponse of(List<? extends Rule> rules) {
        return RuleLookupResponse.builder()
                .rules(rules.stream()
                        .map(RuleData::of)
                        .collect(Collectors.toList()))
                .build();
    }

    @Value
    @Builder
    @EqualsAndHashCode
    private static class RuleData {
        String id;
        String name;

        static RuleData of(Rule rule) {
            return RuleData.builder()
                    .id(rule.getId().toString())
                    .name(rule.getName())
                    .build();
        }
    }
}
