package gusl.rule;

import gusl.model.Identifiable;
import lombok.*;

@Getter
@Setter
@Builder(toBuilder = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RuleProcessEvent implements Identifiable<String> {
    private String id;
    private String data;

}
