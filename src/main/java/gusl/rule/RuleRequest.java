package gusl.rule;

import lombok.*;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Data
public class RuleRequest {
    private String id;

    @NotBlank
    private String name;

    private RuleConditions when;
    private RuleActions then;
    private RuleStatus status;
    private RuleCategory category;

//    private LocalDateTime modified;
//    private LocalDateTime created;
//    private long version;

}
