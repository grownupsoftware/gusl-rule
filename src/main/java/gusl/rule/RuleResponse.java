package gusl.rule;

import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@ToString
@Data
@EqualsAndHashCode
public class RuleResponse {

    private String id;
    private String name;
    private RuleConditions when;
    private RuleActions then;
    private RuleStatus status;
    private RuleCategory category;
    private LocalDateTime modified;
    private LocalDateTime created;
}
