package gusl.rule;

public enum RuleStatus {
    ACTIVE,
    IN_ACTIVE;

    public boolean isNotActive() {
        return this == IN_ACTIVE;
    }

    public boolean isActive() {
        return this == ACTIVE;
    }

}
