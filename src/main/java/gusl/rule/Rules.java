package gusl.rule;

import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Rules {

    @Singular
    private List<RuleDO> rules;
}
