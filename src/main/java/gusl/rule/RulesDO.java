package gusl.rule;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RulesDO {

    @ToStringCount
    @Singular
    private List<RuleDO> rules;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
