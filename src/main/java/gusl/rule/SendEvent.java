package gusl.rule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SendEvent {
    private SendEventType type;
    private String name;
    private FromToMappings fieldMappings;
    private String topic;
    private String key;

    @Override
    public String toString() {
        return "SendEvent " + type + " " + name;
    }
}
