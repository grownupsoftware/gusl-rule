package gusl.rule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateAction {
    private String name;
    private UpdateActionType type;
    private FromToMappings keys;
    private FromToMappings values;

    @Override
    public String toString() {
        return "UpdateAction " + type + " " + name;
    }

}
