package gusl.rule.cache;

import gusl.rule.RuleDO;
import gusl.rule.RulesDO;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

@Contract
public interface RuleCache {

    RuleDO findById(Long id);

    List<RuleDO> getAll();

    List<RuleDO> getAllForCategory(String category);

    List<RuleDO> getAllActive();

    void load(String filename);

    void load(RulesDO rules);

    void save(String filename);
}
