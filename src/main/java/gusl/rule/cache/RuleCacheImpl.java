package gusl.rule.cache;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.rule.RuleDO;
import gusl.rule.RuleStatus;
import gusl.rule.RulesDO;
import org.jvnet.hk2.annotations.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.nonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toUnmodifiableList;

@Service
public class RuleCacheImpl implements RuleCache {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    ObjectMapper theObjectMapper = ObjectMapperFactory.getDefaultObjectMapper();

    Map<String, RuleDO> theRuleMap = new HashMap<>();

    public RuleDO findById(Long id) {
        return theRuleMap.get(id);
    }

    public List<RuleDO> getAll() {
        return safeStream(theRuleMap.values()).collect(toUnmodifiableList());
    }

    public List<RuleDO> getAllForCategory(String category) {
        return safeStream(theRuleMap.values())
                .filter(rule -> nonNull(rule.getCategory()) && rule.getCategory().equals(category))
                .collect(toUnmodifiableList());
    }

    public List<RuleDO> getAllActive() {
        return safeStream(theRuleMap.values())
                .filter(rule -> nonNull(rule.getStatus()) && rule.getStatus() == RuleStatus.ACTIVE)
                .collect(toUnmodifiableList());
    }

    public void load(String filename) {
        try {
            try (InputStream is = getClass().getClassLoader().getResourceAsStream(filename)) {
                load(theObjectMapper.readValue(is, RulesDO.class));
            }
        } catch (IOException e) {
            logger.error("Failed to read from file {} ", filename, e);
        }
    }

    public void load(RulesDO rules) {
        if (nonNull(rules)) {
            Map<String, RuleDO> mapRules = safeStream(rules.getRules())
                    .collect(toMap(RuleDO::getId, identity()));
            theRuleMap.putAll(mapRules);
            logger.info("loaded {} rules", mapRules.size());
        }
    }

    public void save(String filename) {
        try {
            theObjectMapper.writerWithDefaultPrettyPrinter()
                    .writeValue(new File(filename), RulesDO.builder().rules(getAll()).build());
        } catch (IOException e) {
            logger.error("Failed to save file: {}", filename, e);
        }

    }

}
