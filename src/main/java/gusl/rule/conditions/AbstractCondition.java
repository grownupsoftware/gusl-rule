package gusl.rule.conditions;

import java.util.Objects;

import static gusl.rule.conditions.RuleValue.*;
import static java.util.Objects.isNull;

public abstract class AbstractCondition<T extends Comparable<T>> {

    public boolean passes(
            final boolean detailLogging,
            final StringBuilder builder,
            final String fieldName,
            final T ruleValue,
            final ConditionOperand operand,
            final T checkParam
    ) {
        boolean passes = false;
        switch (operand) {
            case EQUAL:
                passes = Objects.equals(ruleValue, checkParam);
                break;
            case NOT_EQUAL:
                passes = !Objects.equals(ruleValue, checkParam);
                break;
            case GREATER_THAN:
                passes = diff(ruleValue, checkParam) > 0;
                break;
            case GREATER_THAN_INCLUSIVE:
                passes = diff(ruleValue, checkParam) >= 0;
                break;
            case LESS_THAN:
                passes = diff(ruleValue, checkParam) < 0;
                break;
            case LESS_THAN_INCLUSIVE:
                passes = diff(ruleValue, checkParam) <= 0;
                break;
        }
        if (detailLogging) {
            builder.append(DBL_TAB)
                    .append(fieldName).append(": ")
                    .append(ruleValue)
                    .append(SPACE)
                    .append(operand)
                    .append(SPACE)
                    .append(checkParam)
                    .append(" passes: ")
                    .append(passes)
                    .append(NL);
        }
        return passes;
    }

    private int diff(T value, T param) {
        // param > value return 1; param < value return -1
        if (isNull(value)) {
            return isNull(param) ? 0 : 1;
        } else {
            return isNull(param) ? -1 : param.compareTo(value);
        }
    }

}
