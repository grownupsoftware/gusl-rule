package gusl.rule.conditions;

import lombok.*;

import static java.util.Objects.isNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class BooleanCondition extends AbstractCondition<Boolean> implements RuleValue<Boolean> {

    private Boolean conditionValue;

    @Override
    public boolean passes(
            final boolean detailLogging,
            final StringBuilder builder,
            final String fieldName,
            final ConditionOperand operand,
            final Object param
    ) {
        // log.info("Boolean ")
        return passes(detailLogging, builder, fieldName,conditionValue, operand, parse(param));
    }

    @Override
    public Boolean parse(final Object value) {
        if (isNull(value)) {
            return null;
        }
        try {
            return Boolean.parseBoolean(value.toString());
        } catch (Throwable e) {
            return null;
        }
    }

}
