package gusl.rule.conditions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class Condition<T> {

    private String id;

    @NotBlank
    private String name;

    @NotBlank
    private String fieldName;

    @NotBlank
    private ConditionOperand operand;

    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            include = JsonTypeInfo.As.PROPERTY,
            property = "type")
    @JsonSubTypes({
            @JsonSubTypes.Type(value = IntegerCondition.class, name = "gusl.rule.conditions.IntegerCondition"),
            @JsonSubTypes.Type(value = LongCondition.class, name = "gusl.rule.conditions.LongerCondition"),
            @JsonSubTypes.Type(value = StringCondition.class, name = "gusl.rule.conditions.StringCondition"),
            @JsonSubTypes.Type(value = DateCondition.class, name = "gusl.rule.conditions.DateCondition"),
            @JsonSubTypes.Type(value = ZonedDateTimeCondition.class, name = "gusl.rule.conditions.ZonedDateTimeCondition"),
            @JsonSubTypes.Type(value = BooleanCondition.class, name = "gusl.rule.conditions.BooleanCondition")
    })
    private RuleValue<T> value;

}
