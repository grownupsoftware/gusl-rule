package gusl.rule.conditions;

import gusl.core.exceptions.GUSLErrorException;
import gusl.rule.errors.RuleErrors;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

public class ConditionFactory {

    public static Condition<String> createCondition(String name, String fieldName, ConditionOperand operand) throws GUSLErrorException {
        if (operand != ConditionOperand.IS_NULL && operand != ConditionOperand.NOT_NULL) {
            throw RuleErrors.INVALID_OPERAND.generateException("null operand for " + operand.name());
        }
        return Condition.<String>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(StringCondition.builder()
                        .conditionValue(null)
                        .build())
                .build();
    }

    public static Condition<Integer> createCondition(String name, String fieldName, ConditionOperand operand, Integer value) {
        return Condition.<Integer>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(IntegerCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<Long> createCondition(String name, String fieldName, ConditionOperand operand, Long value) {
        return Condition.<Long>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(LongCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<String> createCondition(String name, String fieldName, ConditionOperand operand, String value) {
        return Condition.<String>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(StringCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<Date> createCondition(String name, String fieldName, ConditionOperand operand, Date value) {
        return Condition.<Date>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(DateCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<ZonedDateTime> createCondition(String name, String fieldName, ConditionOperand operand, ZonedDateTime value) {
        return Condition.<ZonedDateTime>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(ZonedDateTimeCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<LocalDate> createCondition(String name, String fieldName, ConditionOperand operand, LocalDate value) {
        return Condition.<LocalDate>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(LocalDateCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<LocalDateTime> createCondition(String name, String fieldName, ConditionOperand operand, LocalDateTime value) {
        return Condition.<LocalDateTime>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(LocalDateTimeCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<Boolean> createCondition(String name, String fieldName, ConditionOperand operand, Boolean value) {
        return Condition.<Boolean>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(BooleanCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

    public static Condition<Boolean> createCondition(String name, String fieldName, ConditionOperand operand, boolean value) {
        return Condition.<Boolean>builder()
                .name(name)
                .fieldName(fieldName)
                .operand(operand)
                .value(BooleanCondition.builder()
                        .conditionValue(value)
                        .build())
                .build();
    }

}
