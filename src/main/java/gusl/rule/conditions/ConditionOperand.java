package gusl.rule.conditions;

public enum ConditionOperand {
    EQUAL,
    NOT_EQUAL,
    GREATER_THAN,
    GREATER_THAN_INCLUSIVE,
    LESS_THAN,
    LESS_THAN_INCLUSIVE,
    IS_NULL,
    NOT_NULL
}
