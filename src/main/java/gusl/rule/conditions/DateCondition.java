package gusl.rule.conditions;

import lombok.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.Objects.isNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class DateCondition extends AbstractCondition<Date> implements RuleValue<Date> {

    private Date conditionValue;

    private static ThreadLocal<SimpleDateFormat> DATE_FORMATTER = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));

    @Override
    public boolean passes(
            final boolean detailLogging,
            final StringBuilder builder,
            final String fieldName,
            final ConditionOperand operand,
            final Object param
    ) {
        return passes(detailLogging, builder,fieldName, conditionValue, operand, parse(param));
    }

    @Override
    public Date parse(final Object value) {
        try {
            if (value instanceof Date) {
                return (Date) value;
            }

            return isNull(value) ? null : DATE_FORMATTER.get().parse(value.toString());
        } catch (ParseException e) {
            return null;
        }
    }

}
