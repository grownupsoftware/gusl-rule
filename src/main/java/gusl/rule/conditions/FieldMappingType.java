package gusl.rule.conditions;

public enum FieldMappingType {
    STRING,
    INTEGER,
    LONG,
    BOOLEAN,
    DOUBLE,
    LOCAL_DATE_TIME,
    MONEY
}
