package gusl.rule.conditions;

import lombok.*;

import static java.util.Objects.isNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class IntegerCondition extends AbstractCondition<Integer> implements RuleValue<Integer> {

    private Integer conditionValue;

    @Override
    public boolean passes(
            final boolean detailLogging,
            final StringBuilder builder,
            final String fieldName,
            final ConditionOperand operand,
            final Object param
    ) {
        return passes(detailLogging, builder,fieldName, conditionValue, operand, parse(param));
    }

    @Override
    public Integer parse(final Object value) {
        if (isNull(value)) {
            return null;
        }
        try {
            return Integer.parseInt(value.toString());
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
