package gusl.rule.conditions;

import lombok.*;

import static java.util.Objects.isNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class LongCondition extends AbstractCondition<Long> implements RuleValue<Long> {

    private Long conditionValue;

    @Override
    public boolean passes(
            final boolean detailLogging,
            final StringBuilder builder,
            final String fieldName,
            final ConditionOperand operand,
            final Object param
    ) {
        return passes(detailLogging, builder, fieldName,conditionValue, operand, parse(param));
    }

    @Override
    public Long parse(final Object value) {
        if (isNull(value)) {
            return null;
        }
        try {
            return Long.parseLong(value.toString());
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
