package gusl.rule.conditions;

public interface RuleValue<T> {

    String TAB = "\t";
    String DBL_TAB = "\t\t";
    String NL = "\n";
    String SPACE = " ";

    T getConditionValue();

    void setConditionValue(T conditionValue);

    boolean passes(boolean detailLogging, StringBuilder builder, String fieldName, ConditionOperand operand, Object checkParam);

    T parse(Object value);
}
