package gusl.rule.conditions;

import lombok.*;

import static java.util.Objects.isNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class StringCondition extends AbstractCondition<String> implements RuleValue<String> {

    private String conditionValue;

    @Override
    public boolean passes(
            final boolean detailLogging,
            final StringBuilder builder,
            final String fieldName,
            final ConditionOperand operand,
            final Object param
    ) {
        return passes(detailLogging, builder,fieldName, conditionValue, operand, parse(param));
    }

    @Override
    public String parse(final Object value) {
        return isNull(value) ? null : value.toString();
    }

}
