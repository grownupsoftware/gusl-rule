package gusl.rule.conditions;

import lombok.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;

import static java.util.Objects.isNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ZonedDateTimeCondition implements RuleValue<ZonedDateTime> {

    private ZonedDateTime conditionValue;

    @Override
    public boolean passes(
            final boolean detailLogging,
            final StringBuilder builder,
            final String fieldName,
            final ConditionOperand operand,
            final Object checkParam
    ) {
        boolean passes = false;
        ZonedDateTime param = parse(checkParam);
        switch (operand) {
            case EQUAL:
                passes = Objects.equals(conditionValue, param);
                break;
            case NOT_EQUAL:
                passes = !Objects.equals(conditionValue, param);
                break;
            case GREATER_THAN:
                passes = diff(conditionValue, param) > 0;
                break;
            case GREATER_THAN_INCLUSIVE:
                passes = diff(conditionValue, param) >= 0;
                break;
            case LESS_THAN:
                passes = diff(conditionValue, param) < 0;
                break;
            case LESS_THAN_INCLUSIVE:
                passes = diff(conditionValue, param) <= 0;
                break;
        }
        if (detailLogging) {
            builder.append(DBL_TAB)
                    .append(fieldName).append(": ")
                    .append(conditionValue)
                    .append(SPACE)
                    .append(operand)
                    .append(SPACE)
                    .append(checkParam)
                    .append(" passes: ")
                    .append(passes)
                    .append(NL);
        }
        return passes;
    }

    private int diff(ZonedDateTime value, ZonedDateTime param) {
        // param > value return 1; param < value return -1
        if (isNull(value)) {
            return isNull(param) ? 0 : 1;
        } else {
            return isNull(param) ? -1 : param.compareTo(value);
        }
    }

    @Override
    public ZonedDateTime parse(Object value) {
        try {
            if (value instanceof ZonedDateTime) {
                return (ZonedDateTime) value;
            }

            return isNull(value)
                    ? null
                    : LocalDateTime.parse(value.toString(), DateTimeFormatter.ISO_DATE_TIME).atZone(ZoneId.systemDefault());
        } catch (DateTimeParseException e) {
            return null;
        }
    }

}
