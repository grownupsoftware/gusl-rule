package gusl.rule.converter;

import gusl.node.converter.AbstractModelConfigurator;
import gusl.node.converter.ModelConverterConfigurator;
import ma.glasnost.orika.MapperFactory;
import org.jvnet.hk2.annotations.Service;

@Service
public class RuleConfigurator extends AbstractModelConfigurator implements ModelConverterConfigurator {

    @Override
    public void configureMapperFactory(MapperFactory mapperFactory) {

    }
}
