package gusl.rule.engine;

import gusl.rule.RuleActions;
import gusl.rule.RuleConditions;
import gusl.rule.RuleDO;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Contract
public interface RuleEngine {

    boolean passes(final RuleConditions ruleCondition, final Map<String, Object> data);

    Optional<RuleActions> passes(final RuleDO rule, final Map<String, Object> data);

    List<RuleActions> apply(final Map<String, Object> data);

    List<RuleActions> apply(String category, final Map<String, Object> data);

}
