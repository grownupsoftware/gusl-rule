package gusl.rule.engine;

import gusl.core.lambda.MutableBoolean;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.rule.RuleActions;
import gusl.rule.RuleConditions;
import gusl.rule.RuleDO;
import gusl.rule.cache.RuleCache;
import gusl.rule.conditions.Condition;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toUnmodifiableList;

@Service
public class RuleEngineImpl implements RuleEngine {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    private RuleCache theRuleCache;

    @Override
    public boolean passes(final RuleConditions ruleCondition, final Map<String, Object> data) {
        logger.info("Condition: {} data:{}", ruleCondition, data);

        if (isNull(ruleCondition)) {
            // I suppose if no conditions then it should pass
            return true;
        }
        if (checkConditions(ruleCondition.getConditions(), data)) {
            return true;
        }
        if (isNull(ruleCondition.getOr())
                || isNull(ruleCondition.getOr())
                || ruleCondition.getOr().getConditions().isEmpty()) {
            return false;
        }
        return checkConditions(ruleCondition.getOr().getConditions(), data);
    }

    @Override
    public Optional<RuleActions> passes(RuleDO rule, Map<String, Object> data) {
        if (nonNull(rule) && passes(rule.getWhenConditions(), data)) {
            return Optional.of(rule.getThenActions());
        }
        return Optional.empty();
    }

    @Override
    public List<RuleActions> apply(Map<String, Object> data) {
        return apply(theRuleCache.getAllActive(), data);
    }

    @Override
    public List<RuleActions> apply(String category, Map<String, Object> data) {
        return apply(theRuleCache.getAllForCategory(category), data);
    }

    private List<RuleActions> apply(List<RuleDO> rules, Map<String, Object> data) {
        return safeStream(rules)
                .map(rule -> passes(rule, data))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toUnmodifiableList());
    }

    private boolean checkConditions(final List<Condition<?>> conditions, final Map<String, Object> data) {
        MutableBoolean passes = new MutableBoolean(true);
        StringBuilder builder = new StringBuilder();
        safeStream(conditions)
                .forEach(condition -> {
                    if (!condition.getValue().passes(true, builder, condition.getFieldName(), condition.getOperand(), data.get(condition.getFieldName()))) {
                        passes.set(false);
                    }
                });
        return passes.get();
    }
}
