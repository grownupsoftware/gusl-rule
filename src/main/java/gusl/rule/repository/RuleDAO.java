package gusl.rule.repository;

import gusl.model.dao.DAOInterface;
import gusl.rule.RuleDO;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface RuleDAO extends DAOInterface<RuleDO> {
}
