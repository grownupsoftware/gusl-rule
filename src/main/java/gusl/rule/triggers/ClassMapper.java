package gusl.rule.triggers;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import gusl.model.money.MoneyDO;
import gusl.rule.FieldMapping;
import gusl.rule.FieldMappings;
import gusl.rule.conditions.FieldMappingType;
import gusl.rule.data.MoneyMixIn;
import lombok.CustomLog;
import org.apache.logging.log4j.util.Strings;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@CustomLog
public class ClassMapper {

    private static final Cache<String, FieldMappings> theFieldsCache = CacheBuilder.newBuilder().maximumSize(50).build();
    private static final Cache<String, Class<?>> theMixInCache = CacheBuilder.newBuilder().maximumSize(50).build();

    private ClassMapper() {
    }

    public static FieldMappings createMappingsFor(Class<?> clazz) {
        return getDataFieldsFor(clazz);
    }

    public static FieldMappings createMappingsFor(Class<?>... clazz) {
        List<FieldMappings> mappings = new ArrayList<>(clazz.length);
        for (int i = 0; i < clazz.length; i++) {
            mappings.add(getDataFieldsFor(clazz[i]));
        }
        return new FieldMappings();
    }

    public static FieldMappings intersect(List<FieldMappings> mappings) {
        // Smallest first
        FieldMappings intersect = new FieldMappings();
        mappings.sort(Comparator.comparingInt(o -> o.getMappings().size()));
        for (FieldMapping mapping : mappings.get(0).getMappings()) {
            boolean found = true;
            for (int i = 1; i < mappings.size(); i++) {
                // Needs to exist in all mappings
                if (!mappings.get(i).getMappings().contains(mapping)) {
                    found = false;
                }
            }
            if (found) {
                intersect.addMapping(mapping);
            }
        }

        // Could be empty
        return intersect;
    }

    public static FieldMappings union(List<FieldMappings> mappings) {
        Set<FieldMapping> union = new HashSet<>(mappings.size());
        mappings.forEach(mapping -> union.addAll(mapping.getMappings()));
        return new FieldMappings(new ArrayList<>(union));
    }

    public static FieldMappings getDataFieldsFor(Class<?> clazz) {
        FieldMappings fields = theFieldsCache.getIfPresent(clazz.getName());
        if (fields == null) {
            fields = new FieldMappings();
            walkFields(clazz, fields, "");
            theFieldsCache.put(clazz.getName(), fields);
        }

        return fields;
    }

    public static FieldMappings getDataFieldsFor(Class<?> clazz, boolean withUiNamesOnly) {
        FieldMappings fields = theFieldsCache.getIfPresent(clazz.getName());
        if (fields == null) {
            fields = new FieldMappings();
            walkFields(clazz, fields, "");

            if (withUiNamesOnly) {
                theFieldsCache.put(clazz.getName(), filterUiNamesOnly(fields));
            } else {
                theFieldsCache.put(clazz.getName(), fields);
            }
        }

        return fields;
    }

    private static FieldMappings filterUiNamesOnly(FieldMappings fields) {
        if (isNull(fields.getMappings()) || fields.getMappings().isEmpty()) {
            return fields;
        }
        return FieldMappings.builder().mappings(
                        fields.getMappings().stream()
                                .filter(field -> nonNull(field.getUiName()))
                                .collect(Collectors.toUnmodifiableList()))
                .build();
    }

    private static void walkFields(Class<?> clazz, FieldMappings fields, String prefix) {
        Field[] javaFields = clazz.getDeclaredFields();

        for (int i = 0; i < javaFields.length; i++) {
            Field field = javaFields[i];
            if (field.getName().startsWith("$") || field.getName().equals("serialVersionUID")) {
                continue;
            }
            if (field.isSynthetic() || Modifier.isStatic(field.getModifiers())) {
                continue;
            }

            TriggerField property = field.getAnnotation(TriggerField.class);
            if (nonNull(property) && property.ignore()) {
                continue;
            }

            FieldMappingType mappingType = null;
            switch (field.getType().getCanonicalName()) {
                case "java.lang.String":
                case "javax.money.CurrencyUnit":
                case "org.bson.types.ObjectId":
                    mappingType = FieldMappingType.STRING;
                    break;
                case "java.lang.Long":
                case "long":
                    mappingType = FieldMappingType.LONG;
                    break;
                case "java.lang.Integer":
                case "int":
                    mappingType = FieldMappingType.INTEGER;
                    break;
                case "java.lang.Boolean":
                case "boolean":
                    mappingType = FieldMappingType.BOOLEAN;
                    break;
                case "java.lang.Double":
                case "double":
                case "java.lang.Float":
                case "float":
                case "java.math.BigDecimal":
                    mappingType = FieldMappingType.DOUBLE;
                    break;
                case "java.time.LocalDateTime":
                    mappingType = FieldMappingType.LOCAL_DATE_TIME;
                    break;

                case "com.lottoland.obs.domain.Money":
                    mappingType = FieldMappingType.MONEY;
                    break;
                default:
                    logger.debug("{} - Not too sure what to do with this ..  {}", field.getName(), field.getType().getCanonicalName());
                    // Is there a mixin class
                    Class<?> mixinClass = theMixInCache.getIfPresent(field.getType().getCanonicalName());
                    if (mixinClass != null) {
                        walkFields(mixinClass, fields, getFieldName(prefix, field));
                    } else {
                        walkFields(field.getType(), fields, getFieldName(prefix, field));
                    }
            }

            if (mappingType != null) {
                fields.addMapping(FieldMapping.builder().field(getFieldName(prefix, field)).type(mappingType).uiName(getUiName(field)).build());
            }
        }

        if (clazz.getSuperclass() != null) {
            walkFields(clazz.getSuperclass(), fields, prefix);
        }
    }

    public static String getFieldName(String prefix, Field field) {
        if (Strings.isNotBlank(prefix)) {
            return prefix + "." + getFieldName(field);
        }
        return getFieldName(field);
    }

    public static String getFieldName(Field field) {
        return field.getName();
//        String name = field.getName();
//
//        // Have they renamed it
//        TriggerField property = field.getAnnotation(TriggerField.class);
//        if (property != null) {
//            name = property.name();
//        }
//
//        return name;
    }

    public static String getUiName(Field field) {
        TriggerField property = field.getAnnotation(TriggerField.class);
        return isNull(property) ? null : property.uiName();
    }

    public static void register(Class<MoneyMixIn> moneyMixInClass, Class<MoneyDO> moneyClass) {
        theMixInCache.put(moneyClass.getCanonicalName(), moneyMixInClass);
    }
}
