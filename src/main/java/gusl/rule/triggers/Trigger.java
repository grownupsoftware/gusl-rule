package gusl.rule.triggers;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Trigger {

    private String name;
    // This might be better as a schema
    private Class<?> clazz;

}
