package gusl.rule.triggers;

import gusl.rule.conditions.FieldMappingType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface TriggerField {

    String uiName() default "";

    boolean notNull() default false;

    FieldMappingType type() default FieldMappingType.STRING;

    boolean ignore() default false;
}
