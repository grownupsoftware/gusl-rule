package gusl.rule.triggers;

import gusl.model.money.MoneyDO;
import gusl.rule.FieldMappings;
import gusl.rule.data.MoneyMixIn;
import lombok.AllArgsConstructor;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;
import org.reflections.Reflections;

import javax.annotation.PostConstruct;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeCollection;

@CustomLog
@Service
@AllArgsConstructor
public class TriggerRegistry {

    private static final String OBS_PACKAGE = "com.lottoland.obs";

    private final HashMap<String, Trigger> triggers = new HashMap<>();
//    private static final String KAFKA_DEPOSIT = "Kafka Deposit";
//    private static final String KAFKA_LOGIN = "Kafka Login";

    @PostConstruct
    public void initialise() {
        register();

//        triggers.put(EnhancedDepositEvent.TRIGGER_NAME, Trigger.builder()
//                .name(EnhancedDepositEvent.TRIGGER_NAME)
//                .clazz(EnhancedDepositEvent.class)
//                .build());
//        triggers.put(KAFKA_DEPOSIT, Trigger.builder()
//                .name(KAFKA_DEPOSIT)
//                .clazz(DepositEvent.class)
//                .build());
//        triggers.put(KAFKA_LOGIN, Trigger.builder()
//                .name(KAFKA_LOGIN)
//                .clazz(LoginEvent.class)
//                .build());

        // Register MixIn Classes with the ClassMapper
        ClassMapper.register(MoneyMixIn.class, MoneyDO.class);
    }

    public Optional<Trigger> getTrigger(String name) {
        return Optional.ofNullable(triggers.get(name));
    }

    public Triggers getAllTriggers() {
        return new Triggers(new ArrayList<>(triggers.values()));
    }

    public FieldMappings getFieldMappingFor(String triggerName) {
        Trigger trigger = triggers.get(triggerName);
        if (trigger == null) {
            return new FieldMappings();
        }
        return ClassMapper.getDataFieldsFor(trigger.getClazz(), true); // << want only fields tagged as ui
    }

    private void register() {

        for (Class bean : safeCollection(getClassesAnnotatedWith(TriggerData.class))) {
            TriggerData ann = (TriggerData) bean.getDeclaredAnnotation(TriggerData.class);
            triggers.put(ann.name(), Trigger.builder().name(ann.name()).clazz(bean).build());
        }

        logger.info("Registered {}", triggers.values());
    }

    public Set<Class> getClassesAnnotatedWith(Class<? extends Annotation> annotation) {
        return new Reflections
                (OBS_PACKAGE).getTypesAnnotatedWith(annotation).stream()
                .filter(c -> !c.isMemberClass()).collect(Collectors.toSet());
    }

    public FieldMappings getRuleFields() {
        // only Player (PlayerRuleData) at the moment
        return getFieldMappingFor("Player");
    }
}
