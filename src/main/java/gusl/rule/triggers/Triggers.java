package gusl.rule.triggers;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Triggers {

    private List<Trigger> triggers;

}
